'use strict';

/**
 * @ngdoc function
 * @name blueprintApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the blueprintApp
 */
angular.module('blueprintApp')
  .controller('MainCtrl', function () {
      var vm=this;
      vm.progressValue=0;
      vm..showProgress=false;
  });
